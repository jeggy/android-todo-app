package dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Entities;

/**
 * Created by mikjensen on 16/05/16.
 */
public class User
{
    private String fullname;
    private int age;
    private String username;
    private String token;

    public User(String fullname, int age, String username, String token)
    {
        this.fullname = fullname;
        this.age = age;
        this.username = username;
        this.token = token;
    }

    public String getFullname()
    {
        return fullname;
    }

    public int getAge()
    {
        return age;
    }

    public String getUsername()
    {
        return username;
    }

    public String getToken()
    {
        return token;
    }
}
