package dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Activities;

/**
 * Created by mikjensen on 16/05/16.
 */
import android.app.ActivityGroup;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Entities.User;
import dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Tools.SessionManager;
import dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Tools.TabBitmap;
import dk.dat14a.mj.android_todo_app.R;

public class MainActivity extends ActivityGroup
{
    // Tabbar stuff http://karn-neelmani.blogspot.dk/2013/10/tab-bar-like-iphone-in-android.html

    private static final String TAG_1 = "tab1";
    private static final String TAG_2 = "tab2";
    private static final String TAG_3 = "tab3";

    SessionManager session;
    User user;

    TabHost mTabHost;

    public void onCreate(Bundle savedInstanceState)
    {
        session = new SessionManager(getApplicationContext());
        session.CheckLoggedIn();
        user = session.GetUserData();

        //Log.e("User:", user.getFullname() + ", " + user.getToken());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setTabs();
    }

    private void setTabs()
    {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this.getLocalActivityManager());
        Intent intent;

        intent = new Intent().setClass(this, TodoActivity.class);
        addTab("Todo", TAG_1, createTabDrawable(R.drawable.ic_list), intent);

        intent = new Intent().setClass(this, SettingsActivity.class);
        addTab("Indstillinger", TAG_2, createTabDrawable(R.drawable.ic_settings), intent);

        intent = new Intent().setClass(this, LogoutActivity.class);
        addTab("Log ud", TAG_3, createTabDrawable(R.drawable.ic_logout), intent);
    }

    private Drawable createTabDrawable(int resId)
    {
        Resources res = getResources();
        StateListDrawable states = new StateListDrawable();

        final Options options = new Options();
        options.inPreferredConfig = Config.ARGB_8888;

        Bitmap icon = BitmapFactory.decodeResource(res, resId, options);

        Bitmap unselected = TabBitmap.createUnselectedBitmap(res, icon);
        Bitmap selected = TabBitmap.createSelectedBitmap(res, icon);

        icon.recycle();

        states.addState(new int[] { android.R.attr.state_selected }, new BitmapDrawable(res, selected));
        states.addState(new int[]{android.R.attr.state_enabled}, new BitmapDrawable(res, unselected));

        return states;
    }

    private View createTabIndicator(String label, Drawable drawable)
    {
        View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator, mTabHost.getTabWidget(), false);

        TextView txtTitle = (TextView) tabIndicator.findViewById(R.id.text_view_tab_title);
        txtTitle.setText(label);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) txtTitle.getLayoutParams();
        txtTitle.setLayoutParams(params);

        ImageView imgIcon = (ImageView) tabIndicator.findViewById(R.id.image_view_tab_icon);
        imgIcon.setImageDrawable(drawable);

        return tabIndicator;
    }

    private void addTab(String label, String tag, Drawable drawable, Intent intent)
    {
        TabHost.TabSpec spec = mTabHost.newTabSpec(tag);
        spec.setIndicator(createTabIndicator(label, drawable));
        spec.setContent(intent);

        mTabHost.addTab(spec);
    }
    public void onBackPressed(){
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("EXIT", true);
        startActivity(new Intent(this, LoginActivity.class));
    }
}
