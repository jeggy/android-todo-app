package dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Tools.SessionManager;
import dk.dat14a.mj.android_todo_app.R;

public class LogoutActivity extends AppCompatActivity
{
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);

        session = new SessionManager(getApplicationContext());
    }
    public void logoutAction(View view)
    {
        session.logoutUser();
    }
}
