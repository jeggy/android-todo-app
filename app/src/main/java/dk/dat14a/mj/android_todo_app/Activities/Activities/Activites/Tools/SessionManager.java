package dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Tools;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Activities.LoginActivity;
import dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Entities.User;

/**
 * Created by mikjensen on 16/05/16.
 */
public class SessionManager
{
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;

    int private_mode = 0;

    private static final String PREF_NAME = "todoPref";
    private static final String IS_LOGIN = "Er Logget Ind";

    public SessionManager(Context context)
    {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, private_mode);
        editor = pref.edit();
    }
    public void CreateLoginSession(User user)
    {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        editor.putBoolean(IS_LOGIN, true);
        editor.putString("User", json);
        editor.commit();
    }
    public boolean IsLoggedin()
    {
        return pref.getBoolean(IS_LOGIN, false);
    }
    public void CheckLoggedIn()
    {
        if(!this.IsLoggedin())
        {
            Intent i = new Intent(context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
    public User GetUserData()
    {
        Gson gson = new Gson();
        String json = pref.getString("User", "");
        User user = gson.fromJson(json, User.class);
        return user;
    }
    public void logoutUser()
    {
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
