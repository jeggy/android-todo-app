package dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Entities;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mikjensen on 16/05/16.
 */
public class Todo
{
    private String id;
    private String title;
    private boolean achieved;
    private Date date;
    private ArrayList<Todo> children;
    private Todo parent;
    private Todo root;

    public Todo(String id, String title, boolean achieved, Date date)
    {
        this.id = id;
        this.title = title;
        this.achieved = achieved;
        this.date = date;
    }

    public Todo(String id, String title, boolean achieved, Date date, ArrayList<Todo> children, Todo parent, Todo root)
    {
        this.id = id;
        this.title = title;
        this.achieved = achieved;
        this.date = date;
        this.children = children;
        this.parent = parent;
        this.root = root;
    }
    public void addChild(Todo child)
    {
        this.children.add(child);
    }
}
