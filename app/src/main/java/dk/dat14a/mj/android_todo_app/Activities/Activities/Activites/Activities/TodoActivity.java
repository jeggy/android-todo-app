package dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import dk.dat14a.mj.android_todo_app.R;

public class TodoActivity extends Activity
{
    String[] mobileArray = {"Android","IPhone","WindowsMobile","Blackberry","WebOS","Ubuntu","Windows7","Max OS X"};
//    LayoutInflater layoutInflater;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

//        View listItem = layoutInflater.inflate(R.layout.listview_todo, null);
//        Log.e("test", listItem.toString());

//        ImageView achievedImage = (ImageView)listItem.findViewById(R.id.achieved);
//        achievedImage.setBackgroundResource(R.drawable.ic_checked);

        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.listview_todo, R.id.cellField, mobileArray);

        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
    }

//    @Override
//    protected void onListItemClick(ListView l, View v, int position, long id)
//    {
//        super.onListItemClick(l, v, position, id);
//
//        // ListView Clicked item index
//        int itemPosition = position;
//
//        // ListView Clicked item value
//        String itemValue = (String)l.getItemAtPosition(position);
//
//        Log.e("Item selected: ", "Position: "+itemPosition+"ListItem : " +itemValue);
//    }
}
