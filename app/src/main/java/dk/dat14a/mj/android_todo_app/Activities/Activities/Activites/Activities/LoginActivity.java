package dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Entities.User;
import dk.dat14a.mj.android_todo_app.Activities.Activities.Activites.Tools.SessionManager;
import dk.dat14a.mj.android_todo_app.R;

public class LoginActivity extends Activity
{
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new SessionManager(getApplicationContext());
    }
    public void loginAction(View view)
    {
        session.CreateLoginSession(new User("Mik Jensen", 32, "mikj84@gmail.com", "12345"));
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
